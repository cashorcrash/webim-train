# Вопрос
### Создание

Пример объекта

```
const question = {
     author: '5f44f0184c05746280d7085b',   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
     question: 'Процент по кредитной  карте?',
     answer: 'Да'
   }
```

Пример запроса

```
sendRequest('POST', '/create_question', question)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    question: {
        answer: "Он есть"
        author: "5f44f0184c05746280d7085b"   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
        id: "b4e77db5-80c2-492e-b134-cdce209f0404"
        lastUpdate: "27.08.2020 11:38:29"
        question: "Процент по кредитной  карте?"
        type: "Вопрос"
        __v: 0
        _id: "5f47710519b4001510911592"
    },
    status: "Question has been created"
}
```

### Удаление

Пример объекта

```
const removeQuestion = {
     id: 'bf903030-b587-4631-b1d9-0b1e4fc712a7'   <--- id элемента
  }
```

Пример запроса

```
sendRequest('POST', '/remove_question', removeQuestion)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    status: "Question has been removed"
}
```

### Редактирование

Пример объекта

```
const editQuestion = {
       id: 'bf903030-b587-4631-b1d9-0b1e4fc712a7',   <--- id элемента
       question: 'Новый вопрос?',
       answer: 'Новый ответ'
    }
```

Пример запроса

```
sendRequest('POST', '/edit_question', editQuestion)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: {
        answer: "setsetestestsetsetsetsetsetset"
        author: "5f451c1f3d25160004dda615"   
        id: "bf903030-b587-4631-b1d9-0b1e4fc712a7"
        lastUpdate: "27.08.2020 12:11:23"
        question: "tetsetestestsetsetsetsetse"
        type: "Вопрос"
        __v: 0
        _id: "5f46ef4025dece000429fa05"
    },
    status: "Question has been edited"
}
```

### Получение всех вопросов пользователя

Пример объекта

```
const userId = {
     author: '5f451c1f3d25160004dda615'   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
   }
```

Пример запроса

```
sendRequest('POST', '/get_questions', userId)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {_id: "5f46ef4025dece000429fa05", id: "bf903030-b587-4631-b1d9-0b1e4fc712a7", type: "Вопрос", lastUpdate: "27.08.2020 12:11:23", author: "5f451c1f3d25160004dda615", …},
        {_id: "5f46ef9e25dece000429fa07", id: "0f66b083-03ae-40fb-a6f5-adba5286b84c", type: "Вопрос", lastUpdate: "26.08.2020 11:26:22", author: "5f451c1f3d25160004dda615", …},
        {_id: "5f46f33425dece000429fa08", id: "f14887eb-5630-4bfe-94a7-b0d93d4af283", type: "Вопрос", lastUpdate: "26.08.2020 11:41:40", author: "5f451c1f3d25160004dda615", …}
        ],
    status: "Questions were found"
}
```

### Получение одного вопроса

Пример объекта

```
const questionGetOne = {
      id: 'f14887eb-5630-4bfe-94a7-b0d93d4af283'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/get_questions_one', questionGetOne)
            .then(data => console.log(data)))
```

Возвращаемый результат

```
{
    result: [
        {
            answer: "rur"
            author: "5f451c1f3d25160004dda615"
            id: "f14887eb-5630-4bfe-94a7-b0d93d4af283"
            lastUpdate: "26.08.2020 11:41:40"
            question: "tyi"
            type: "Вопрос"
            __v: 0
            _id: "5f46f33425dece000429fa08"
        }
    ],
    status: "Question were found"
}
```


# Возражение
### Создание

Пример объекта

```
const decline = {
      author: '5f44f0184c05746280d7085b',   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
      decline: 'Нет, спасибо',
      answer: 'Ну пожалуйста!'
    }
```

Пример запроса

```
sendRequest('POST', '/create_decline', decline)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    decline: {
        answer: "Ну пожалуйста!"
        author: "5f44f0184c05746280d7085b"
        decline: "Нет, спасибо"
        id: "038c5874-9275-4bda-bd41-e6e206d81c90"
        lastUpdate: "27.08.2020 12:29:43"
        type: "Возражение"
        __v: 0
        _id: "5f477d0796e03c16d43c3e90"
    },
    status: "Decline has been created"
}
```

### Удаление

Пример объекта

```
const removeDecline = {
      id: '038c5874-9275-4bda-bd41-e6e206d81c90'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/remove_decline', removeDecline)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    status: "Decline has been removed"
}
```

### Редактирование

Пример объекта

```
const editDecline = {
      id: 'e7f9da4b-ddf1-4def-b777-8ed3b874c409',   <--- id элемента
      decline: 'Новое возражение!',
      answer: 'Новый ответ'
   }
```

Пример запроса

```
sendRequest('POST', '/edit_decline', editDecline)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: {
        answer: "Новый ответ"
        author: "5f44f0184c05746280d7085b"
        decline: "Новое возражение!"
        id: "e7f9da4b-ddf1-4def-b777-8ed3b874c409"
        lastUpdate: "27.08.2020 12:36:01"
        type: "Возражение"
        __v: 0
        _id: "5f477d38d879483f54d08882"
    },
    status: "Decline has been edited"
}
```

### Получение всех возражений пользователя

Пример объекта

```
const userId = {
      author: '5f44f0184c05746280d7085b'   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
    }
```

Пример запроса

```
sendRequest('POST', '/get_declines', userId)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {_id: "5f4638c8b51ed10d401b8bf5", id: "8f4727f3-7802-4891-8087-3853654692d8", type: "Возражение", lastUpdate: "26.08.2020 1:26:16", author: "5f44f0184c05746280d7085b", …},
        {_id: "5f46394793be425cb8c5d376", id: "991f4ab6-1bd0-4b36-a287-28cabf76c72a", type: "Возражение", lastUpdate: "26.08.2020 1:28:23", author: "5f44f0184c05746280d7085b", …},
        {_id: "5f46396f93be425cb8c5d377", id: "b1db9c32-dc85-430e-bd37-9a9b7d66e176", type: "Возражение", lastUpdate: "26.08.2020 1:29:03", author: "5f44f0184c05746280d7085b", …},
        {_id: "5f4648ae81f56b305452a346", id: "1cf50451-f160-4107-a6cf-3ec31f4d4bdc", type: "Возражение", lastUpdate: "26.08.2020 2:48:59", author: "5f44f0184c05746280d7085b", …},
        {_id: "5f477d38d879483f54d08882", id: "e7f9da4b-ddf1-4def-b777-8ed3b874c409", type: "Возражение", lastUpdate: "27.08.2020 12:36:01", author: "5f44f0184c05746280d7085b", …}
    ],
    status: "Decline were found"
}
```

### Получение одного возражения пользователя

Пример объекта

```
const getDeclineOne = {
      id: 'e7f9da4b-ddf1-4def-b777-8ed3b874c409'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/get_decline_one', getDeclineOne)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {
            answer: "Новый ответ"
            author: "5f44f0184c05746280d7085b"
            decline: "Новое возражение!"
            id: "e7f9da4b-ddf1-4def-b777-8ed3b874c409"
            lastUpdate: "27.08.2020 12:36:01"
            type: "Возражение"
            __v: 0
            _id: "5f477d38d879483f54d08882"
        }
    ],
    status: "Decline were found"
}
```

# Диалог (скрипт)
### Создание

Пример объекта

```
const chatScript = {
     author: '5f44f0184c05746280d7085b',   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
     content: []
  }
```

Пример запроса

```
sendRequest('POST', '/create_dialogue_script', chatScript)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    dialogue: {
        author: "5f44f0184c05746280d7085b"
        content: []
        id: "6f1db8fb-6ad1-4567-b9db-59400d487b3d"
        lastUpdate: "27.08.2020 12:46:56"
        type: "Диалог (скрипт)"
        __v: 0
        _id: "5f478110186c392d246fd8ba"
    },
    status: "Dialogue has been created"
}
```

### Удаление

Пример объекта

```
const removeDialogueScript = {
     id: '917fd115-a263-4c17-b0ee-15a389276d3a'   <--- id элемента
  }
```

Пример запроса

```
sendRequest('POST', '/remove_dialogue_script', removeDialogueScript)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    status: "Dialogue has been removed"
}
```

### Редактирование

Пример объекта

```
const updateDialogueScriptItems = {
       id: '75d7ec3d-f808-4c10-b25c-dac3da203a9c',   <--- id элемента
       items: [
           {
               client: 'Не надо',
               operator: 'Надо'
           },
           {
               client: 'Ну ладно',
               operator: 'Отлично'
           }
       ]
     }
```

Пример запроса

```
sendRequest('POST', '/update_items_dialogue_script', updateDialogueScriptItems)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: {
        author: "5f44f0184c05746280d7085b"
        content: (2) [{…}, {…}]
        id: "75d7ec3d-f808-4c10-b25c-dac3da203a9c"
        lastUpdate: "27.08.2020 12:46:41"
        type: "Диалог (скрипт)"
        __v: 1
        _id: "5f4781015b7ec807b4b08e4d"
    },
    status: "Item/s has been updated"
}
```

### Получение всех диалогов пользователя (скрипт)

Пример объекта

```
const userId = {
       author: '5f44f0184c05746280d7085b'   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
     }
```

Пример запроса

```
sendRequest('POST', '/get_dialogue_scripts', userId)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result:[
        {content: Array(2), _id: "5f4781015b7ec807b4b08e4d", id: "75d7ec3d-f808-4c10-b25c-dac3da203a9c", type: "Диалог (скрипт)", lastUpdate: "27.08.2020 12:46:41", …},
        {content: Array(0), _id: "5f478110186c392d246fd8ba", id: "6f1db8fb-6ad1-4567-b9db-59400d487b3d", type: "Диалог (скрипт)", lastUpdate: "27.08.2020 12:46:56", …}
    ],
    status: "Dialogue scripts were found"
}
```

### Получение одного диалога пользователя (скрипт)

Пример объекта

```
const dialogueScriptGetOne = {
       id: '75d7ec3d-f808-4c10-b25c-dac3da203a9c'   <--- id элемента
     }
```

Пример запроса

```
sendRequest('POST', '/get_dialogue_scripts_one', dialogueScriptGetOne)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: {
        author: "5f44f0184c05746280d7085b"
        content: (2) [{…}, {…}]
        id: "75d7ec3d-f808-4c10-b25c-dac3da203a9c"
        lastUpdate: "27.08.2020 12:46:41"
        type: "Диалог (скрипт)"
        __v: 1
        _id: "5f4781015b7ec807b4b08e4d"
    },
    status: "Dialogue scripts were found"
}
```

# Вопрос (система)
### Создание

Пример объекта

```
const chatSystem = {
       author: '5f44f0184c05746280d7085b',   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
       question: 'Что в системе?',
       answer: 'Ничего'
     }
```

Пример запроса

```
sendRequest('POST', '/create_question_system', chatSystem)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    question: {
        answer: "Ничего"
        author: "5f44f0184c05746280d7085b"
        id: "73c00ab0-29b3-4d3a-8e31-b3971cbfd2aa"
        lastUpdate: "27.08.2020 1:33:39"
        question: "Что в системе?"
        type: "Вопрос (система)"
        __v: 0
        _id: "5f478c03a6e1942794d95d90"
    },
    status: "Question has been created"
}
```

### Удаление

Пример объекта

```
const removeQuestionSystem = {
       id: '73c00ab0-29b3-4d3a-8e31-b3971cbfd2aa'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/remove_question_system', removeQuestionSystem)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    status: "Question has been removed"
}
```

### Редактирование

Пример объекта

```
const editQuestionSystem = {
       id: 'daa30fc0-be79-423f-8482-a1b5710399e5',   <--- id элемента
       question: 'Где WebOffice?',
       answer: 'Перед тобой'
     }
```

Пример запроса

```
sendRequest('POST', '/edit_question_system', editQuestionSystem)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: {
        answer: "Перед тобой"
        author: "5f44f0184c05746280d7085b"
        id: "daa30fc0-be79-423f-8482-a1b5710399e5"
        lastUpdate: "27.08.2020 1:39:10"
        question: "Где WebOffice?"
        type: "Вопрос (система)"
        __v: 0
        _id: "5f478d27af170c3eb8d381ea"
    },
    status: "Question system has been edited"
}
```

### Получение всех вопросов пользователя (система)

Пример объекта

```
const userId = {
       author: '5f44f0184c05746280d7085b'   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
   }
```

Пример запроса

```
sendRequest('POST', '/get_questions_systems', userId)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {_id: "5f478d27af170c3eb8d381ea", id: "daa30fc0-be79-423f-8482-a1b5710399e5", type: "Вопрос (система)", lastUpdate: "27.08.2020 1:39:10", author: "5f44f0184c05746280d7085b", …}
    ],
    status: "Questions systems were found"
}
```

### Получение одного вопроса пользователя (система)

Пример объекта

```
const questionSystemGetOne = {
       id: 'daa30fc0-be79-423f-8482-a1b5710399e5'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/get_question_systems_one', questionSystemGetOne)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {
            answer: "Перед тобой"
            author: "5f44f0184c05746280d7085b"
            id: "daa30fc0-be79-423f-8482-a1b5710399e5"
            lastUpdate: "27.08.2020 1:39:10"
            question: "Где WebOffice?"
            type: "Вопрос (система)"
            __v: 0
            _id: "5f478d27af170c3eb8d381ea"
        }
    ],
    status: "Dialogue systems were found"
}
```

# Диалог (2 чата)
### Создание

Пример объекта

```
const twoChats = {
       author: '5f44f0184c05746280d7085b',   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
       content: []
   }
```

Пример запроса

```
sendRequest('POST', '/create_dialogue_two_chats', twoChats)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    dialogue: {
        author: "5f44f0184c05746280d7085b"
        content: []
        id: "773477df-8b7c-42fa-ad36-a170d08bb5dd"
        lastUpdate: "27.08.2020 1:46:30"
        type: "Диалог (2 чата)"
        __v: 0
        _id: "5f478f068f223c3828e908e9"
    },
    status: "Dialogue has been created"
}
```

### Удаление

Пример объекта

```
const removeDialogueTwoChats = {
       id: '773477df-8b7c-42fa-ad36-a170d08bb5dd'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/remove_dialogue_two_chats', removeDialogueTwoChats)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    status: "Dialogue has been removed"
}
```

### Получение всех диалогов пользователя (2 чата)

Пример объекта

```
const userId = {
       author: '5f44f0184c05746280d7085b'   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
   }
```

Пример запроса

```
sendRequest('POST', '/get_dialogue_two_chats', userId)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {content: Array(0), _id: "5f478fb15f1b4923840012fe", id: "e333e2de-af94-49ed-91b5-0d23989221f3", type: "Диалог (2 чата)", lastUpdate: "27.08.2020 1:49:21", …}
    ],
    status: "Dialogue two chats were found"
}
```

### Получение одного диалога пользователя (2 чата)

Пример объекта

```
const dialogueTwoChatsGetOne = {
       id: 'e333e2de-af94-49ed-91b5-0d23989221f3'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/get_dialogue_two_chats_one', dialogueTwoChatsGetOne)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {
            author: "5f44f0184c05746280d7085b"
            content: []
            id: "e333e2de-af94-49ed-91b5-0d23989221f3"
            lastUpdate: "27.08.2020 1:49:21"
            type: "Диалог (2 чата)"
            __v: 0
            _id: "5f478fb15f1b4923840012fe"
        }
    ],
    status: "Dialogue two chats were found"
}
```

# Вопрос с шаблонами ответов 
### Создание

Пример объекта

```
const questionTemplate = {
       author: '5f44f0184c05746280d7085b',   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
       question: 'Вопрос?'
   }
```

Пример запроса

```
sendRequest('POST', '/create_question_template', questionTemplate)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    question: {
        answers: []
        author: "5f44f0184c05746280d7085b"
        id: "46d04763-9f5d-48f5-97e7-284b100ef96d"
        lastUpdate: "27.08.2020 1:56:55"
        question: "Вопрос?"
        type: "Вопрос (шаблоны)"
        __v: 0
        _id: "5f47917733e0153c284916a2"
    },
    status: "Question has been created"
}
```

### Удаление

Пример объекта

```
const questionTemplateRemove = {
       id: '3bbfb23d-4624-4b07-97ee-8c7d72e5b340'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/remove_question_template', questionTemplateRemove)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    status: "Question has been removed"
}
```

### Редактирование

Пример объекта

```
const updateQuestionTemplateItems = {
       id: '46d04763-9f5d-48f5-97e7-284b100ef96d',   <--- id элемента
       question: 'Новый вопрос?',
       answers: [
           {
               text: 'Обновленный ответ',
               type: 'incorrect'
           },
           {
               text: 'Новый ответ',
               type: 'correct'
           }
       ]
     }
```

Пример запроса

```
sendRequest('POST', '/update_items_question_template', updateQuestionTemplateItems)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: {
        answers: (2) [{…}, {…}]
        author: "5f44f0184c05746280d7085b"
        id: "46d04763-9f5d-48f5-97e7-284b100ef96d"
        lastUpdate: "27.08.2020 1:56:55"
        question: "Новый вопрос?"
        type: "Вопрос (шаблоны)"
        __v: 1
        _id: "5f47917733e0153c284916a2"
    },
    status: "Item/s has been updated"
}
```

### Получение всех вопросов с шаблонами ответов пользователя

Пример объекта

```
const userId = {
       author: '5f44f0184c05746280d7085b'   <----- в бд указано как _id: ObjectId("5f44f0184c05746280d7085b")
   }
```

Пример запроса

```
sendRequest('POST', '/get_questions_templates', userId)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {answers: Array(1), _id: "5f4769de2dbe325440b590c4", id: "f8b18bc3-98ac-4c88-b5f4-d4d39ee65754", type: "Вопрос (шаблоны)", lastUpdate: "27.08.2020 11:07:58", …},
        {answers: Array(2), _id: "5f47917733e0153c284916a2", id: "46d04763-9f5d-48f5-97e7-284b100ef96d", type: "Вопрос (шаблоны)", lastUpdate: "27.08.2020 1:56:55", …}
    ],
    status: "Questions template were found" 
}
```

### Получение одного вопроса с шаблонами ответов пользователя

Пример объекта

```
const questionTemplateGetOne = {
       id: '46d04763-9f5d-48f5-97e7-284b100ef96d'   <--- id элемента
   }
```

Пример запроса

```
sendRequest('POST', '/get_questions_templates_one', questionTemplateGetOne)
            .then(data => console.log(data))
```

Возвращаемый результат

```
{
    result: [
        {answers: Array(2), _id: "5f47917733e0153c284916a2", id: "46d04763-9f5d-48f5-97e7-284b100ef96d", type: "Вопрос (шаблоны)", lastUpdate: "27.08.2020 1:56:55", …}
    ],
    status: "Question template were found"
}
```
