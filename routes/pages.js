const { Router } = require('express');
const router = Router();
const path = require('path');
const fs = require('fs-extra');
const copydir = require('copy-dir');
const archiver = require('archiver');
const rimraf = require("rimraf");
const LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

// Auth page
router.get('/', function (request, response) {
    response.sendFile(path.join(__dirname, '../public/pages/auth/index.html'));
});

// Profile page
router.get('/profile', function (request, response) {
    response.sendFile(path.join(__dirname, '../public/pages/profile/index.html'));
});

router.get('/render_preview_page', function (request, response) {
    response.sendFile(path.join(__dirname, `../public/preview/${localStorage.getItem('preview-chat')}.html`));
});

// Preview page
router.post('/preview_chat', function (request, response) {
    const data =
        `
            <!DOCTYPE html>
            <html lang="en">
            
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
                <link rel="shortcut icon" sizes="32x32" href="https://acdn.tinkoff.ru/params/common_front/resourses/icons/favicon-32x32.png">
                <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap" rel="stylesheet">
                <title>Webim</title>   
            <link href="/public/preview/style.css" rel="stylesheet"></head>
            
            <body>
                <div id="root"></div>
                <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
                <script>
                    const user = JSON.parse(localStorage.getItem('webim_user_id'));

                    setTimeout(() => {
                        localStorage.setItem('current-case', JSON.stringify('${request.body.id}'));
                        localStorage.setItem('webim_user_id', JSON.stringify(user));
                    }, 1000);
                </script>
            <script type="text/javascript" src="/public/preview/index.js"></script></body>
            
            </html>
            `;

    fs.writeFile(__dirname + '../../' + `/public/preview/${request.body.id}.html`, data, function (err) {
        if (err) {
            return console.log(err);
        }
    });

    localStorage.setItem('preview-chat', request.body.id);
    response.send({ status: true, folder: `/render_preview_page` });
});


// Download chat
router.post('/create_chat_archive', function (request, response) {
    const data =
        `
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
                integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
            <link rel="shortcut icon" sizes="32x32"
                href="https://acdn.tinkoff.ru/params/common_front/resourses/icons/favicon-32x32.png">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400&display=swap" rel="stylesheet">
            <title>Webim</title>
            <script type="text/javascript" src="../js/jquery.js"></script>
            <script type="text/javascript" src="../js/scorm_websoft.js"></script>
            <script type="text/javascript" src="../js/module.js"></script>
            <script type="text/javascript">
                CheckF5();
                var sVersion = "2004";
                setTimeout(start, 2000);
            </script>
            <link href="style.css" rel="stylesheet">
        </head>

        <body onload="ISCORM_INITIALIZE(); start(); CheckSS();" onunload="ISCORM_TERMINATE()"
            onbeforeunload="ISCORM_TERMINATE()">
            <div id="root"></div>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
            </script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
            </script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
            </script>
            <script>
                const user = JSON.parse(localStorage.getItem('webim_user_id'));

                setTimeout(() => {
                    localStorage.setItem('current-case', JSON.stringify('d98bea11-43ea-422b-85b6-f68af8d5a32f'));
                    localStorage.setItem('webim_user_id', JSON.stringify(user));
                }, 1000);
            </script>
            <script type="text/javascript" src="index.js"></script>
        </body>

        </html>
            `;

    fs.mkdir(__dirname + '../../' + `/public/downloads/${request.body.id}`, function () { });

    fs.copy(__dirname + '../../' + `/public/downloads/temp`, __dirname + '../../' + `/public/downloads/${request.body.id}`, err => {
        if (err) return console.error(err);
        console.log('success!');
    });

    setTimeout(() => {
        fs.writeFile(__dirname + '../../' + `/public/downloads/${request.body.id}/res/index.html`, data, function (err) {
            if (err) {
                return console.log(err);
            }

            setTimeout(() => {
                const output = fs.createWriteStream(__dirname + '../../' + `/public/downloads/${request.body.id}.zip`);
                const archive = archiver('zip', {
                    zlib: { level: 9 }
                });

                archive.pipe(output);

                archive.directory(__dirname + '../../' + `/public/downloads/${request.body.id}`, false);

                output.on('close', function () {
                    console.log(archive.pointer() + ' total bytes');
                    console.log('archiver has been finalized and the output file descriptor has closed.');
                });

                output.on('end', function () {
                    console.log('Data has been drained');
                });

                archive.finalize();

                response.send({ folder: `/public/downloads/${request.body.id}.zip` });
            }, 5000);
        });
    }, 2000);
});

// Take zip
router.post('/download_zip', function (request, response) {
    const output = fs.createWriteStream(__dirname + '../../' + `/public/downloads/${request.body.id}.zip`);
    const archive = archiver('zip', {
        zlib: { level: 9 }
    });

    archive.pipe(output);

    archive.directory(__dirname + '../../' + `/public/downloads/${request.body.id}`, false);

    output.on('close', function () {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });

    output.on('end', function () {
        console.log('Data has been drained');
    });

    archive.finalize();

    response.send({ folder: `/public/downloads/${request.body.id}.zip` });
});

// Clear download zip folders
router.post('/clear_zip', function (request, response) {
    fs.unlink(__dirname + '../../' + `/public/downloads/${request.body.id}.zip`, (err) => {
        if (err) {
            console.error(err)
            return
        }
    });

    rimraf.sync(__dirname + '../../' + `/public/downloads/${request.body.id}`);

    response.send(request.body);
});

// Show chat page
router.post('/show_chat', function (request, response) {
    response.redirect(path.join(__dirname, `../public/preview/${request.body.folder}/index.html`));
    // response.send({ status: true, folder: request.body.id });
});


// Preview page
router.get('/preview', function (request, response) {
    response.sendFile(path.join(__dirname, '../public/pages/chat/index.html'));
});

// Test page
router.get('/test', function (request, response) {
    response.sendFile(path.join(__dirname, '../public/pages/test/index.html'));
});

//Download case
router.get('/download/:case', function (request, response) {
    response.sendFile(path.join(__dirname, `../public/downloads/${request.params.case}.zip`));
});

module.exports = router;