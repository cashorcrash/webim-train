const { Router } = require('express');
const router = Router();
const newDialogueScript = require('../../models/newDialogueScript');
const getDateInString = require('../../utils/dateInString');
const { v4: uuidv4 } = require('uuid');

// Dialogue script update items
router.post('/update_items_dialogue_script', function (request, response) {
    newDialogueScript.findOne({ id: request.body.id }, (err, dialogueScript) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueScript === null) {
            response.send({ status: 'Dialogue script not found' });
        } else {
            request.body.items.forEach(item => {
                item.itemId = uuidv4();
            });

            dialogueScript.title = request.body.title;
            dialogueScript.content = request.body.items;

            async function updateDialogueScript() {
                try {
                    await dialogueScript.save();
                    response.send({ status: 'Item/s has been updated', result: dialogueScript });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateDialogueScript();
        }
    });
});

module.exports = router;




