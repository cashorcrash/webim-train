const { Router } = require('express');
const router = Router();
const newQuestion = require('../../models/newQuestion');

// Remove question
router.post('/remove_question', function (request, response) {
    newQuestion.deleteOne({ id: request.body.id }, (err, questions) => {
        if (questions === undefined) {
            response.send({ status: 'Question not found' });
        } else {
            response.send({ status: 'Question has been removed' });
        }
    });
});

module.exports = router;

