const { Router } = require('express');
const router = Router();
const newCase = require('../../models/newCase');

// Get case one
router.post('/get_case_one', function (request, response) {
    newCase.find({ id: request.body.id }, (err, cases) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (cases === null) {
            response.send({ status: 'Case not found' });
        } else {
            response.send({ status: 'Case were found', case: cases });
        }
    });
});

module.exports = router;