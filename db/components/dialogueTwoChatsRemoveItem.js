const { Router } = require('express');
const router = Router();
const newDialogueTwoChats = require('../../models/newDialogueTwoChats');
const getDateInString = require('../../utils/dateInString');
const { v4: uuidv4 } = require('uuid');

// Dialogue two chats remove item
router.post('/remove_item_dialogue_two_chats', function (request, response) {
    newDialogueTwoChats.findOne({ id: request.body.id }, (err, dialogueTwoChatsItem) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueTwoChatsItem === null) {
            response.send({ status: 'Dialogue two chats not found' });
        } else {
            dialogueTwoChatsItem.content.forEach((item, i, a) => {
                if (item.itemId === request.body.itemId) {
                    a.splice(i, 1);
                }
            });

            async function updateDialogueTwoChats() {
                try {
                    await dialogueTwoChatsItem.save();
                    response.send({ status: 'Item has been removed', result: dialogueTwoChatsItem });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateDialogueTwoChats();
        }
    });
});

module.exports = router;