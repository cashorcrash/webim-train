const { Router } = require('express');
const router = Router();
const newCase = require('../../models/newCase');
const { v4: uuidv4 } = require('uuid');
const getDateInString = require('../../utils/dateInString');

// Create case
router.post('/create_case', function (request, response) {
    newCase.findOne({ id: request.body.id }, (err, cases) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (cases !== null) {
            response.send({ status: 'Case already exists' });
        } else {
            const caseNew = new newCase({
                id: uuidv4(),
                type: 'Кейс',
                lastUpdate: getDateInString(new Date()),
                author: request.body.author,
                title: request.body.title,
                description: request.body.description,
                chats: []
            });

            async function createCase() {
                await caseNew.save();
                response.send({ status: 'Case has been created', case: caseNew });
            }

            createCase();
        }
    });
});

module.exports = router;