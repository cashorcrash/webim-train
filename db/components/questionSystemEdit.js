const { Router } = require('express');
const router = Router();
const newQuestionSystem = require('../../models/newQuestionSystem');
const getDateInString = require('../../utils/dateInString');
const { v4: uuidv4 } = require('uuid');

// Question system edit
router.post('/edit_question_system', function (request, response) {
    newQuestionSystem.findOne({ id: request.body.id }, (err, questionSystem) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionSystem === null) {
            response.send({ status: 'Dialogue system not found' });
        } else {
            questionSystem.lastUpdate = getDateInString(new Date());
            questionSystem.question = request.body.question;
            questionSystem.answer = request.body.answer;

            async function updateQuestionSystem() {
                try {
                    await questionSystem.save();
                    response.send({ status: 'Question system has been edited', result: questionSystem });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateQuestionSystem();
        }
    });
});

module.exports = router;