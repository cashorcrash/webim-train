const { Router } = require('express');
const router = Router();
const newQuestionTemplate= require('../../models/newQuestionTemplate');
const getDateInString = require('../../utils/dateInString');
const { v4: uuidv4 } = require('uuid');

// Question template remove item
router.post('/remove_item_question_template', function (request, response) {
    newQuestionTemplate.findOne({ id: request.body.id }, (err, questionTemplateItem) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionTemplateItem === null) {
            response.send({ status: 'Question template not found' });
        } else {
            questionTemplateItem.answers.forEach((item, i, a) => {
                if (item.itemId === request.body.itemId) {
                    a.splice(i, 1);
                }
            });

            async function updateQuestionTemplate() {
                try {
                    await questionTemplateItem.save();
                    response.send({ status: 'Item has been removed', result: questionTemplateItem });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateQuestionTemplate();
        }
    });
});

module.exports = router;