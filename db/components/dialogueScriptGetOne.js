const { Router } = require('express');
const router = Router();
const newDialogueScript = require('../../models/newDialogueScript');

// Get dialogue scripts one
router.post('/get_dialogue_scripts_one', function (request, response) {
    newDialogueScript.find({ id: request.body.id }, (err, dialogueScriptsOne) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueScriptsOne === null) {
            response.send({ status: 'Dialogue scripts not found' });
        } else {
            response.send({ status: 'Dialogue scripts were found', result: dialogueScriptsOne });
        }
    });
});

module.exports = router;


