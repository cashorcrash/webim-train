const { Router } = require('express');
const router = Router();
const newDecline = require('../../models/newDecline');
const getDateInString = require('../../utils/dateInString');

// Edit decline
router.post('/edit_decline', function (request, response) {
    newDecline.findOne({ id: request.body.id }, (err, declines) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (declines === null) {
            response.send({ status: 'Decline not found' });
        } else {
            declines.lastUpdate = getDateInString(new Date());
            declines.decline = request.body.decline;
            declines.answer = request.body.answer;

            async function updateDecline() {
                try {
                    await declines.save();
                    response.send({ status: 'Decline has been edited', result: declines });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateDecline();
        }
    });
});

module.exports = router;