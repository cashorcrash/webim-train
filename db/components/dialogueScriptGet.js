const { Router } = require('express');
const router = Router();
const newDialogueScript = require('../../models/newDialogueScript');

// Get dialogue scripts
router.post('/get_dialogue_scripts', function (request, response) {
    newDialogueScript.find({ author: request.body.author }, (err, dialogueScripts) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueScripts === null) {
            response.send({ status: 'Dialogue scripts not found' });
        } else {
            response.send({ status: 'Dialogue scripts were found', result: dialogueScripts });
        }
    });
});

module.exports = router;


