const { Router } = require('express');
const router = Router();
const newDecline = require('../../models/newDecline');

// Get decline one
router.post('/get_decline_one', function (request, response) {
    newDecline.find({ id: request.body.id }, (err, decline) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (decline === null) {
            response.send({ status: 'Declines not found' });
        } else {
            response.send({ status: 'Decline were found', result: decline });
        }
    });
});

module.exports = router;