const { Router } = require('express');
const router = Router();
const newCase = require('../../models/newCase');

// Remove case
router.post('/remove_case', function (request, response) {
    newCase.deleteOne({ id: request.body.id }, (err, cases) => {
        if (cases === undefined) {
            response.send({ status: 'Case not found' });
        } else {
            response.send({ status: 'Case has been removed' });
        }
    });
});

module.exports = router;