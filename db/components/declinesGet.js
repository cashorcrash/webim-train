const { Router } = require('express');
const router = Router();
const newDecline = require('../../models/newDecline');

// Get declines
router.post('/get_declines', function (request, response) {
    newDecline.find({ author: request.body.author }, (err, declines) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (declines === null) {
            response.send({ status: 'Declines not found' });
        } else {
            response.send({ status: 'Decline were found', result: declines });
        }
    });
});

module.exports = router;