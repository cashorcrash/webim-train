const { Router } = require('express');
const router = Router();
const newDecline = require('../../models/newDecline');
const { v4: uuidv4 } = require('uuid');
const getDateInString = require('../../utils/dateInString');

// Create decline
router.post('/create_decline', function (request, response) {
    newDecline.findOne({ id: request.body.id }, (err, declines) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (declines !== null) {
            response.send({ status: 'Decline already exists' });
        } else {
            const decline = new newDecline({
                id: uuidv4(),
                type: 'Возражение',
                lastUpdate: getDateInString(new Date()),
                author: request.body.author,
                decline: request.body.decline,
                answer: request.body.answer
            });

            async function createCourse() {
                await decline.save();
                response.send({ status: 'Decline has been created', decline: decline });
            }

            createCourse();
        }
    });
});

module.exports = router;