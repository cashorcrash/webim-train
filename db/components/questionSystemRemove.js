const { Router } = require('express');
const router = Router();
const newQuestionSystem = require('../../models/newQuestionSystem');

// Remove question system
router.post('/remove_question_system', function (request, response) {
    newQuestionSystem.deleteOne({ id: request.body.id }, (err, questionSystem) => {
        if (questionSystem === undefined) {
            response.send({ status: 'Question not found' });
        } else {
            response.send({ status: 'Question has been removed' });
        }
    });
});

module.exports = router;