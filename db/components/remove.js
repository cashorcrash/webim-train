const { Router } = require('express');
const router = Router();

// Delete course
router.post('/remove_course', function (request, response) {
     newCourse.deleteOne({ _id: request.body.id }, (err, courses) => {
        if (courses === undefined) {
            response.send({status: 'Course not found'});
        } else {
            response.send({status: 'Course has been removed'});
        }
    });
});

module.exports = router;