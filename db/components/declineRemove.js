const { Router } = require('express');
const router = Router();
const newDecline = require('../../models/newDecline');

// Remove decline
router.post('/remove_decline', function (request, response) {
    newDecline.deleteOne({ id: request.body.id }, (err, declines) => {
        if (declines === undefined) {
            response.send({ status: 'Decline not found' });
        } else {
            response.send({ status: 'Decline has been removed' });
        }
    });
});

module.exports = router;