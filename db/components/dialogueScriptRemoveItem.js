const { Router } = require('express');
const router = Router();
const newDialogueScript = require('../../models/newDialogueScript');
const getDateInString = require('../../utils/dateInString');
const { v4: uuidv4 } = require('uuid');

// Dialogue script remove item
router.post('/remove_item_dialogue_script', function (request, response) {
    newDialogueScript.findOne({ id: request.body.id }, (err, dialogueScriptItem) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueScriptItem === null) {
            response.send({ status: 'Dialogue script not found' });
        } else {
            dialogueScriptItem.content.forEach((item, i, a) => {
                if (item.itemId === request.body.itemId) {
                    a.splice(i, 1);
                }
            });

            async function updateDialogueScript() {
                try {
                    await dialogueScriptItem.save();
                    response.send({ status: 'Item has been removed', result: dialogueScriptItem });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateDialogueScript();
        }
    });
});

module.exports = router;