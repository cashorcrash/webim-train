const { Router } = require('express');
const router = Router();
const newDialogueScript = require('../../models/newDialogueScript');

// Remove dialogue script
router.post('/remove_dialogue_script', function (request, response) {
    newDialogueScript.deleteOne({ id: request.body.id }, (err, dialoguesScripts) => {
        if (dialoguesScripts === undefined) {
            response.send({ status: 'Dialogue not found' });
        } else {
            response.send({ status: 'Dialogue has been removed' });
        }
    });
});

module.exports = router;