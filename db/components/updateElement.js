const { Router } = require('express');
const router = Router();


// Update course element
router.post('/update_element', function (request, response) {
    newCourse.findOne({ _id: request.body.id }, (err, courses) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        request.body.element.elements.forEach(item => {
            courses.content.forEach((cur, i, a) => {
                if (cur.id === item.id) {
                    a.splice(i, 1, item);
                }
            });
        });

        async function updateElement() {
            await courses.save();
            response.send({ status: 'Element has been updated', elements: courses });
        }

        updateElement();
    });
});

module.exports = router;