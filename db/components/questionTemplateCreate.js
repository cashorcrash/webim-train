const { Router } = require('express');
const router = Router();
const newQuestionTemplate = require('../../models/newQuestionTemplate');
const { v4: uuidv4 } = require('uuid');
const getDateInString = require('../../utils/dateInString');

// Create question template
router.post('/create_question_template', function (request, response) {
    newQuestionTemplate.findOne({ id: request.body.id }, (err, questionTemplates) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionTemplates !== null) {
            response.send({ status: 'Question already exists' });
        } else {
            const questionTemplate = new newQuestionTemplate({
                id: uuidv4(),
                type: 'Вопрос (шаблоны)',
                title: request.body.title,
                lastUpdate: getDateInString(new Date()),
                author: request.body.author,
                question: request.body.question,
                answers: []
            });

            async function createCourse() {
                await questionTemplate.save();
                response.send({ status: 'Question has been created', question: questionTemplate });
            }

            createCourse();
        }
    });
});

module.exports = router;