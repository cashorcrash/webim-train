const { Router } = require('express');
const router = Router();
const newQuestionSystem = require('../../models/newQuestionSystem');
const { v4: uuidv4 } = require('uuid');
const getDateInString = require('../../utils/dateInString');

// Create question system
router.post('/create_question_system', function (request, response) {
    newQuestionSystem.findOne({ id: request.body.id }, (err, questionSystems) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionSystems !== null) {
            response.send({ status: 'Dialogue already exists' });
        } else {
            const questionSystem = new newQuestionSystem({
                id: uuidv4(),
                type: 'Вопрос (система)',
                lastUpdate: getDateInString(new Date()),
                author: request.body.author,
                question: request.body.question,
                answer: request.body.answer
            });

            async function createCourse() {
                await questionSystem.save();
                response.send({ status: 'Question has been created', question: questionSystem });
            }

            createCourse();
        }
    });
});

module.exports = router;