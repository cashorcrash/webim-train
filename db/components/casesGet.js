const { Router } = require('express');
const router = Router();
const newCase = require('../../models/newCase');

// Get declines
router.post('/get_cases', function (request, response) {
    newCase.find({ author: request.body.author }, (err, cases) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (cases === null) {
            response.send({ status: 'Cases not found' });
        } else {
            response.send({ status: 'Cases were found', case: cases });
        }
    });
});

module.exports = router;