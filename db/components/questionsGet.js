const { Router } = require('express');
const router = Router();
const newQuestion = require('../../models/newQuestion');

// Get questions
router.post('/get_questions', function (request, response) {
    newQuestion.find({ author: request.body.author }, (err, questions) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questions === null) {
            response.send({ status: 'Questions not found' });
        } else {
            response.send({ status: 'Questions were found', result: questions });
        }
    });
});

module.exports = router;