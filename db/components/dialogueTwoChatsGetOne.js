const { Router } = require('express');
const router = Router();
const newDialogueTwoChats = require('../../models/newDialogueTwoChats');

// Get dialogue two chats one
router.post('/get_dialogue_two_chats_one', function (request, response) {
    newDialogueTwoChats.find({ id: request.body.id }, (err, dialogueTwoChatsOne) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueTwoChatsOne === null) {
            response.send({ status: 'Dialogue two chats not found' });
        } else {
            response.send({ status: 'Dialogue two chats were found', result: dialogueTwoChatsOne });
        }
    });
});

module.exports = router;