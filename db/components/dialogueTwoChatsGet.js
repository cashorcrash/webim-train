const { Router } = require('express');
const router = Router();
const newDialogueTwoChats = require('../../models/newDialogueTwoChats');

// Get dialogue two chats
router.post('/get_dialogue_two_chats', function (request, response) {
    newDialogueTwoChats.find({ author: request.body.author }, (err, dialogueTwoChats) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueTwoChats === null) {
            response.send({ status: 'Dialogue two chats not found' });
        } else {
            response.send({ status: 'Dialogue two chats were found', result: dialogueTwoChats });
        }
    });
});

module.exports = router;