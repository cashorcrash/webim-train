const { Router } = require('express');
const router = Router();
const newQuestionSystem = require('../../models/newQuestionSystem');

// Get question system one
router.post('/get_question_systems_one', function (request, response) {
    newQuestionSystem.find({ id: request.body.id }, (err, questionSystemOne) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionSystemOne === null) {
            response.send({ status: 'Dialogue systems not found' });
        } else {
            response.send({ status: 'Dialogue systems were found', result: questionSystemOne });
        }
    });
});

module.exports = router;