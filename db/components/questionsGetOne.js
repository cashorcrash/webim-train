const { Router } = require('express');
const router = Router();
const newQuestion = require('../../models/newQuestion');

// Get questions one
router.post('/get_questions_one', function (request, response) {
    newQuestion.find({ id: request.body.id }, (err, questionsOne) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionsOne === null) {
            response.send({ status: 'Question not found' });
        } else {
            response.send({ status: 'Question were found', result: questionsOne });
        }
    });
});

module.exports = router;