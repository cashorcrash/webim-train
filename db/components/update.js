const { Router } = require('express');
const router = Router();

// Update course
router.post('/update_course', function (request, response) {
    newCourse.findOne({ _id: request.body.id }, (err, courses) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        courses.title = request.body.title || courses.title;
        courses.description = request.body.description || courses.description;
        courses.updateDate = request.body.updateDate || courses.updateDate;

        async function updateCourse() {
            try {
                await courses.save();
                response.send({ status: 'Course has been updated' });
            } catch (e) {
                response.send(e);
            }
        }

        updateCourse();
    });
});

module.exports = router;