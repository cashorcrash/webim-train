const { Router } = require('express');
const router = Router();
const newDialogueTwoChats = require('../../models/newDialogueTwoChats');
const { v4: uuidv4 } = require('uuid');
const getDateInString = require('../../utils/dateInString');

// Create dialogue two chats
router.post('/create_dialogue_two_chats', function (request, response) {
    newDialogueTwoChats.findOne({ id: request.body.id }, (err, dialogueTwoChats) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueTwoChats !== null) {
            response.send({ status: 'Dialogue already exists' });
        } else {
            const dialogueTwoChatsNew = new newDialogueTwoChats({
                id: uuidv4(),
                type: 'Диалог (2 чата)',
                title: request.body.title,
                lastUpdate: getDateInString(new Date()),
                author: request.body.author,
                content: request.body.content
            });

            async function createCourse() {
                await dialogueTwoChatsNew.save();
                response.send({ status: 'Dialogue has been created', dialogue: dialogueTwoChatsNew });
            }

            createCourse();
        }
    });
});

module.exports = router;