const { Router } = require('express');
const router = Router();
const newDialogueTwoChats = require('../../models/newDialogueTwoChats');
const getDateInString = require('../../utils/dateInString');
const { v4: uuidv4 } = require('uuid');

// Dialogue two chats update items
router.post('/update_items_two_chats', function (request, response) {
    newDialogueTwoChats.findOne({ id: request.body.id }, (err, dialogueTwoChatsItem) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueTwoChatsItem === null) {
            response.send({ status: 'Dialogue two chats not found' });
        } else {
            request.body.items.forEach(item => {
                item.itemId = uuidv4();
            });

            dialogueTwoChatsItem.content = request.body.items;
            dialogueTwoChatsItem.title = request.body.title;

            async function updateDialogueTwoChats() {
                try {
                    await dialogueTwoChatsItem.save();
                    response.send({ status: 'Item/s has been updated', result: dialogueTwoChatsItem });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateDialogueTwoChats();
        }
    });
});

module.exports = router;