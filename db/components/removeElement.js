const { Router } = require('express');
const router = Router();

// Remove course element
router.post('/remove_element', function (request, response) {
    newCourse.findOne({ _id: request.body.id }, (err, courses) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        courses.content.forEach((item, i, a) => {
            if (item.id === request.body.elementId) {
                a.splice(i, 1);
            }
        });

        async function updateCourse() {
            await courses.save();
            response.send({ status: 'Element has been removed' });
        }

        updateCourse();
    });
});

module.exports = router;