const { Router } = require('express');
const router = Router();
const newQuestionTemplate = require('../../models/newQuestionTemplate');

// Remove question template
router.post('/remove_question_template', function (request, response) {
    newQuestionTemplate.deleteOne({ id: request.body.id }, (err, questionsTemplates) => {
        if (questionsTemplates === undefined) {
            response.send({ status: 'Question not found' });
        } else {
            response.send({ status: 'Question has been removed' });
        }
    });
});

module.exports = router;