const { Router } = require('express');
const router = Router();
const newQuestionSystem = require('../../models/newQuestionSystem');

// Get questions system
router.post('/get_questions_systems', function (request, response) {
    newQuestionSystem.find({ author: request.body.author }, (err, questionsSystem) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionsSystem === null) {
            response.send({ status: 'Questions systems not found' });
        } else {
            response.send({ status: 'Questions systems were found', result: questionsSystem });
        }
    });
});

module.exports = router;