const { Router } = require('express');
const router = Router();
const newDialogueTwoChats = require('../../models/newDialogueTwoChats');

// Remove dialogue two chats
router.post('/remove_dialogue_two_chats', function (request, response) {
    newDialogueTwoChats.deleteOne({ id: request.body.id }, (err, dialoguesTwoChats) => {
        if (dialoguesTwoChats === undefined) {
            response.send({ status: 'Dialogue not found' });
        } else {
            response.send({ status: 'Dialogue has been removed' });
        }
    });
});

module.exports = router;