const { Router } = require('express');
const router = Router();
const newQuestion = require('../../models/newQuestion');
const { v4: uuidv4 } = require('uuid');
const getDateInString = require('../../utils/dateInString');

// Create question
router.post('/create_question', function (request, response) {
    newQuestion.findOne({ id: request.body.id }, (err, questions) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questions !== null) {
            response.send({ status: 'Question already exists' });
        } else {
            const question = new newQuestion({
                id: uuidv4(),
                type: 'Вопрос',
                lastUpdate: getDateInString(new Date()),
                author: request.body.author,
                question: request.body.question,
                answer: request.body.answer
            });

            async function createCourse() {
                await question.save();
                response.send({ status: 'Question has been created', question: question });
            }

            createCourse();
        }
    });
});

module.exports = router;



