const { Router } = require('express');
const router = Router();
const newDialogueScript = require('../../models/newDialogueScript');
const { v4: uuidv4 } = require('uuid');
const getDateInString = require('../../utils/dateInString');

// Create dialogue script
router.post('/create_dialogue_script', function (request, response) {
    newDialogueScript.findOne({ id: request.body.id }, (err, dialogueScripts) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (dialogueScripts !== null) {
            response.send({ status: 'Dialogue already exists' });
        } else {
            const dialogueScript = new newDialogueScript({
                id: uuidv4(),
                title: request.body.title,
                type: 'Диалог (скрипт)',
                lastUpdate: getDateInString(new Date()),
                author: request.body.author,
                content: request.body.content
            });

            async function createCourse() {
                await dialogueScript.save();
                response.send({ status: 'Dialogue has been created', dialogue: dialogueScript });
            }

            createCourse();
        }
    });
});

module.exports = router;