const { Router } = require('express');
const router = Router();
const newQuestion = require('../../models/newQuestion');
const getDateInString = require('../../utils/dateInString');

// Edit question
router.post('/edit_question', function (request, response) {
    newQuestion.findOne({ id: request.body.id }, (err, questions) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questions === null) {
            response.send({ status: 'Question not found' });
        } else {
            questions.lastUpdate = getDateInString(new Date());
            questions.question = request.body.question;
            questions.answer = request.body.answer;

            async function updateQuestion() {
                try {
                    await questions.save();
                    response.send({ status: 'Question has been edited', result: questions });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateQuestion();
        }
    });
});

module.exports = router;


