const { Router } = require('express');
const router = Router();
const newQuestionTemplate = require('../../models/newQuestionTemplate');

// Get questions template
router.post('/get_questions_templates', function (request, response) {
    newQuestionTemplate.find({ author: request.body.author }, (err, questionsTemplate) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionsTemplate === null) {
            response.send({ status: 'Questions template not found' });
        } else {
            response.send({ status: 'Questions template were found', result: questionsTemplate });
        }
    });
});

module.exports = router;