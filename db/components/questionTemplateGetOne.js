const { Router } = require('express');
const router = Router();
const newQuestionTemplate = require('../../models/newQuestionTemplate');

// Get questions template one
router.post('/get_questions_templates_one', function (request, response) {
    newQuestionTemplate.find({ id: request.body.id }, (err, questionsTemplateOne) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionsTemplateOne === null) {
            response.send({ status: 'Question template not found' });
        } else {
            response.send({ status: 'Question template were found', result: questionsTemplateOne });
        }
    });
});

module.exports = router;