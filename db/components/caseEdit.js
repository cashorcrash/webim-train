const { Router } = require('express');
const router = Router();
const newCase = require('../../models/newCase');
const getDateInString = require('../../utils/dateInString');

// Edit case
router.post('/edit_case', function (request, response) {
    newCase.findOne({ id: request.body.id }, (err, cases) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (cases === null) {
            response.send({ status: 'Case not found' });
        } else {
            cases.lastUpdate = getDateInString(new Date());
            cases.title = request.body.title || cases.title;
            cases.description = request.body.description || cases.description;
            cases.chats = request.body.chats || cases.chats;

            async function updateCase() {
                try {
                    await cases.save();
                    response.send({ status: 'Case has been edited', case: cases });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateCase();
        }
    });
});

module.exports = router;