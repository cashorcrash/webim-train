const { Router } = require('express');
const router = Router();
const newQuestionTemplate = require('../../models/newQuestionTemplate');
const getDateInString = require('../../utils/dateInString');
const { v4: uuidv4 } = require('uuid');

// Question template update items
router.post('/update_items_question_template', function (request, response) {
    newQuestionTemplate.findOne({ id: request.body.id }, (err, questionTemplate) => {
        if (err) {
            response.send({ status: 'Error. Please try again...' });
        }

        if (questionTemplate === null) {
            response.send({ status: 'Question template not found' });
        } else {
            request.body.answers.forEach(item => {
                item.itemId = uuidv4();
            });

            questionTemplate.question = request.body.question;
            questionTemplate.answers = request.body.answers;
            questionTemplate.title = request.body.title;

            async function updateQuestionTemplate() {
                try {
                    await questionTemplate.save();
                    response.send({ status: 'Item/s has been updated', result: questionTemplate });
                } catch (e) {
                    response.send(e);
                }
            }
            
            updateQuestionTemplate();
        }
    });
});

module.exports = router;