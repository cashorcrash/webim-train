const { Router } = require('express');
const router = Router();
const bcrypt = require('bcrypt');
const newUser = require('../../models/newUser');



// Sign in user
router.post('/sign_in', function (request, response) {
   newUser.findOne({ mail: request.body.mail }, (err, usersData) => {
      if (err) {
         response.send({ status: 'Error. Please try again...' });
      }

      const responseForUser = {
         mail: false,
         password: false,
         id: ''
      };

      console.log(usersData);

      if (usersData === null) {
         response.send({ status: 'User not found' });
      } else {
         responseForUser.mail = true;
         bcrypt.compare(request.body.password, usersData.password, function (err, result) {
            if (err) {
               throw new Error('Wrong!');
            }

            if (result) {
               responseForUser.password = true;
               responseForUser.id = usersData._id;
               responseForUser.firstName = usersData.firstName;
               responseForUser.lastName = usersData.lastName;
               response.send(responseForUser);
            } else {
               response.send(responseForUser);
            }
         });
      }
   });
});

module.exports = router;