const { Router } = require('express');
const router = Router();
const bcrypt = require('bcrypt');
const saltRounds = 15;
const newUser = require('../../models/newUser');
const fs = require('fs-extra');



// Create user
router.post('/create_user', function (request, response) {
   newUser.findOne({ mail: request.body.mail }, (err, usersData) => {
      if (err) {
         response.send({status: 'Error. Please try again...'});
     }

      if (usersData === null) {
         const user = new newUser({
            firstName: request.body.firstName,
            lastName: request.body.lastName,
            mail: request.body.mail,
            password: request.body.password
         });

         bcrypt.hash(user.password, saltRounds, async function (err, hash) {
            user.password = hash;
            await user.save();
            response.send({ status: 'User has been created', info: user });
         });

      } else {
         response.send({ status: 'User already exists' });
      }
   });
});

module.exports = router;