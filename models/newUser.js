const { Schema, model } = require('mongoose');

const newUser = new Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    mail: {
        type: String
    },
    password: {
        type: String
    }
}, { collection: 'users' });

module.exports = model('newUser', newUser);