const { Schema, model } = require('mongoose');

const newQuestionSystem = new Schema({
    id: {
        type: String
    },
    type: {
        type: String
    },
    author: {
        type: String
    },
    question: {
        type: String
    },
    answer: {
        type: String
    },
    lastUpdate: {
        type: String
    }
}, { collection: 'question-system' });

module.exports = model('newQuestionSystem', newQuestionSystem);