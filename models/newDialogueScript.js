const { Schema, model } = require('mongoose');

const newDialogueScript = new Schema({
    id: {
        type: String
    },
    title: {
        type: String
    },
    type: {
        type: String
    },
    author: {
        type: String
    },
    lastUpdate: {
        type: String
    },
    content: {
        type: Array
    }
}, { collection: 'dialogue-script' });

module.exports = model('newDialogueScript', newDialogueScript);