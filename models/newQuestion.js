const { Schema, model } = require('mongoose');

const newQuestion = new Schema({
    id: {
        type: String
    },
    type: {
        type: String
    },
    author: {
        type: String
    },
    lastUpdate: {
        type: String
    },
    question: {
        type: String
    },
    answer: {
        type: String
    }
}, { collection: 'questions' });

module.exports = model('newQuestion', newQuestion);