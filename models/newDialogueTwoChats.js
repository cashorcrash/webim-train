const { Schema, model } = require('mongoose');

const newDialogueTwoChats = new Schema({
    id: {
        type: String
    },
    title: {
        type: String
    },
    type: {
        type: String
    },
    author: {
        type: String
    },
    lastUpdate: {
        type: String
    },
    content: {
        type: Array
    }
}, { collection: 'dialogue-two-chats' });

module.exports = model('newDialogueTwoChats', newDialogueTwoChats);