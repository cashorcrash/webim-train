const { Schema, model } = require('mongoose');

const newQuestionTemplate = new Schema({
    id: {
        type: String
    },
    title: {
        type: String
    },
    type: {
        type: String
    },
    author: {
        type: String
    },
    lastUpdate: {
        type: String
    },
    question: {
        type: String
    },
    answers: {
        type: Array
    }
}, { collection: 'question-template' });

module.exports = model('newQuestionTemplate', newQuestionTemplate);