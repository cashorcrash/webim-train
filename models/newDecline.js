const { Schema, model } = require('mongoose');

const newDecline = new Schema({
    id: {
        type: String
    },
    type: {
        type: String
    },
    author: {
        type: String
    },
    lastUpdate: {
        type: String
    },
    decline: {
        type: String
    },
    answer: {
        type: String
    }
}, { collection: 'declines' });

module.exports = model('newDecline', newDecline);