const { Schema, model } = require('mongoose');

const newCase = new Schema({
    id: {
        type: String
    },
    author: {
        type: String
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    lastUpdate: {
        type: String
    },
    chats: {
        type: Array
    }
}, { collection: 'cases' });

module.exports = model('newCase', newCase);