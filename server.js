const express = require('express');
const cors = require('cors');
const pages = require('./routes/pages');
const userCreate = require('./db/user/create');
const userSignIn = require('./db/user/signIn');
const questionCreate = require('./db/components/questionCreate');
const declineCreate = require('./db/components/declineCreate');
const dialogueTwoChatsCreate = require('./db/components/dialogueTwoChatsCreate');
const questionSystemCreate = require('./db/components/questionSystemCreate');
const dialogueScriptCreate = require('./db/components/dialogueScriptCreate');
const questionTemplateCreate = require('./db/components/questionTemplateCreate');
const questionRemove = require('./db/components/questionRemove');
const declineRemove = require('./db/components/declineRemove');
const dialogueTwoChatsRemove = require('./db/components/dialogueTwoChatsRemove');
const dialogueScriptRemove = require('./db/components/dialogueScriptRemove');
const questionTemplateRemove = require('./db/components/questionTemplateRemove');
const questionSystemRemove = require('./db/components/questionSystemRemove');
const declineGet = require('./db/components/declinesGet');
const questionGet = require('./db/components/questionsGet');
const dialogueScriptGet = require('./db/components/dialogueScriptGet');
const questionSystemGet = require('./db/components/questionSystemGet');
const dialogueTwoChatsGet = require('./db/components/dialogueTwoChatsGet');
const questionTemplateGet = require('./db/components/questionTemplateGet');
const declineEdit = require('./db/components/declineEdit');
const questionEdit = require('./db/components/questionEdit');
const dialogueScriptUpdateItems = require('./db/components/dialogueScriptUpdateItems');
const dialogueScriptRemoveItem = require('./db/components/dialogueScriptRemoveItem');
const questionSystemEdit = require('./db/components/questionSystemEdit');
const dialogueTwoChatsUpdateItems = require('./db/components/dialogueTwoChatsUpdateItems');
const dialogueTwoChatsRemoveItem = require('./db/components/dialogueTwoChatsRemoveItem');
const questionTemplateUpdateItems = require('./db/components/questionTemplateUpdateItems');
const questionTemplateRemoveItem = require('./db/components/questionTemplateRemoveItem');
const declinesGetOne = require('./db/components/declinesGetOne');
const dialogueScriptGetOne = require('./db/components/dialogueScriptGetOne');
const questionSystemGetOne = require('./db/components/questionSystemGetOne');
const dialogueTwoChatsGetOne = require('./db/components/dialogueTwoChatsGetOne');
const questionsGetOne = require('./db/components/questionsGetOne');
const questionTemplateGetOne = require('./db/components/questionTemplateGetOne');
const caseCreate = require('./db/components/caseCreate');
const caseEdit = require('./db/components/caseEdit');
const caseRemove = require('./db/components/caseRemove');
const casesGet = require('./db/components/casesGet');
const casesGetOne = require('./db/components/casesGetOne');
const app = express();
const PORT = process.env.PORT || 8080;
const mongoose = require('mongoose');


//---------Server start-----------

const mongoUrl = 'mongodb+srv://crash4star:3kDVsPXp333@constructordb.wfsfa.mongodb.net/webim?retryWrites=true&w=majority';
async function start() {
    try {
        await mongoose.connect(mongoUrl, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });

        app.listen(PORT, () => {
            console.log(`Server start. Listening on port ${PORT}...`);
        });
    } catch (err) {
        console.error(err);
    }
}
start();

app.use(cors());
app.use('/public', express.static('public'));
app.use('/pages', express.static('pages'));
app.use('/auth', express.static('auth'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(pages);
app.use(userCreate);
app.use(questionCreate);
app.use(declineCreate);
app.use(dialogueTwoChatsCreate);
app.use(questionSystemCreate);
app.use(dialogueScriptCreate);
app.use(questionTemplateCreate);
app.use(userSignIn);
app.use(questionRemove);
app.use(declineRemove);
app.use(dialogueTwoChatsRemove);
app.use(dialogueScriptRemove);
app.use(questionTemplateRemove);
app.use(questionSystemRemove);
app.use(declineGet);
app.use(questionGet);
app.use(dialogueScriptGet);
app.use(questionSystemGet);
app.use(dialogueTwoChatsGet);
app.use(questionTemplateGet);
app.use(declineEdit);
app.use(questionEdit);
app.use(dialogueScriptUpdateItems);
app.use(dialogueScriptRemoveItem);
app.use(questionSystemEdit);
app.use(dialogueTwoChatsUpdateItems);
app.use(dialogueTwoChatsRemoveItem);
app.use(questionTemplateUpdateItems);
app.use(questionTemplateRemoveItem);
app.use(casesGetOne);
app.use(dialogueScriptGetOne);
app.use(questionSystemGetOne);
app.use(dialogueTwoChatsGetOne);
app.use(questionsGetOne);
app.use(questionTemplateGetOne);
app.use(caseCreate);
app.use(caseEdit);
app.use(caseRemove);
app.use(casesGet);
app.use(casesGetOne);
app.use(declinesGetOne);

