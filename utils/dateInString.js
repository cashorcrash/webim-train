function addZeroBeforeValue(value) {
    return (value < 10) ? `0${value}` : value;
}

function getDateInString(date) {
    var d = new Date();
    var time = d.toLocaleTimeString();
    return `${addZeroBeforeValue(date.getDate())}.${addZeroBeforeValue(date.getMonth() + 1)}.${date.getFullYear()} ${time.slice(0,-3)}`;
}

module.exports = getDateInString;